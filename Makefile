unittest: lists/tests.py
	python manage.py test

functionaltest: unittest functional_tests.py
	python functional_tests.py

init: requirements.txt virtualenv/bin/activate  /usr/bin/firefox virtualenv/bin/geckodriver

requirements.txt:
	echo "Activate virtualenv: source virtualenv/bin/activate"
	pip install -r requirements.txt

virtualenv/bin/activate : 
	python3 -m venv virtualenv

/usr/bin/firefox:
	sudo apt-get install firefox

virtualenv/bin/geckodriver:
	wget https://github.com/mozilla/geckodriver/releases/download/v0.24.0/geckodriver-v0.24.0-linux64.tar.gz
	tar xvfz geckodriver-v0.24.0-linux64.tar.gz
	mv geckodriver virtualenv/bin

manage.py:
	django-admin.py startproject superlists .

